from django.contrib.auth.models import User

from network.models import SchoolManage


def is_value_empty(value):
    """
    Функция, которая проверяет пустое ли введенное поле
    :param value: поле для проверки
    :return: True/False
    """
    if value is None or value == "":
        return True
    return False


def is_email_unique(email):
    """
    Функция, которая проверяет уникальная ли почта пользователя
    :param email: почта для проверки
    :return: True/False
    """
    if User.objects.filter(email=email).exists():
        return False
    return True


def is_profile_can_manage_school(profile, school):
    """
    Функция, которая проверяет может ли пользователь управлять школой (либо создатель, либо добавленный администратор)
    :param profile: пользователь для проверки
    :param school: школа для проверки
    :return: True/False
    """
    if school.creator == profile:
        return True

    school_managers = SchoolManage.objects.all().filter(school=school)
    for obj in school_managers:
        if obj.profile == profile:
            return True

    return False
