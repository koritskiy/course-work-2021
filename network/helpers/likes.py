from django.db.models import Q

from network.models import LikeForPost, RateComment


def is_user_liked_post(post, profile):
    """
    Функция, которая проверяет лайкнул ли пользователь пост в ленте школы
    :param post: пост для проверки
    :param profile: профиль для проверки
    :return: True - если пользователь лайкнул пост, False - наоборот
    """
    all_likes_for_post = LikeForPost.objects.all().filter(post=post)

    for like in all_likes_for_post:
        if like.author == profile:
            return True
    return False


def get_all_likes_for_posts(posts, profile):
    """
    Функция, которая считает кол-во лайков к каждому посту
    :param posts: посты для проверки
    :param profile: профиль пользователя, для которого пройдет проверка - лайкнул ли он пост
    :return: список с постами и информацией про пост
             (список вида - id-поста, кол-во лайков к посту, лайкнул ли пользователь этот пост)
    """
    # Считаем лайки к каждому посту
    all_likes_for_posts = list()
    for post in posts:
        all_likes_for_posts.append(
            [post.id, LikeForPost.objects.all().filter(post=post).count(), is_user_liked_post(post, profile)])

    return all_likes_for_posts


def is_user_plus_comment(comment, profile):
    """
    Функция, которая проверяет поставил ли плюс к комментарию пользователь
    :param comment: комментарий для проверки
    :param profile: пользователь
    :return: True/False
    """
    all_likes_for_comment = RateComment.objects.all().filter(comment=comment)

    for like in all_likes_for_comment:
        if like.author == profile and like.is_plus is True:
            return True
    return False


def is_user_minus_comment(comment, profile):
    """
    Функция, которая проверяет поставил ли минус пользователь к комментарию
    :param comment: комментарий для проверки
    :param profile: пользователь для проверки
    :return: True/False
    """
    all_likes_for_comment = RateComment.objects.all().filter(comment=comment)

    for like in all_likes_for_comment:
        if like.author == profile and like.is_plus is False:
            return True
    return False


def get_all_likes_for_comments(comments, profile):
    """
    Функция, которая возвращает кол-во голосов к комментарию
    :param comments: комментарии для проверки
    :param profile: профиль пользователя
    :return: возвращает список вида - [комментарий, кол-во голосов, True/False поставил ли пользователь плюс,
             True/False поставил ли пользователь минус]
    """
    all_likes_for_comments = list()
    for comment in comments:
        plus_votes = RateComment.objects.all().filter(Q(comment=comment), Q(is_plus=True)).count()
        minus_votes = RateComment.objects.all().filter(Q(comment=comment), Q(is_plus=False)).count()

        all_likes_for_comments.append([comment.id, plus_votes - minus_votes, is_user_plus_comment(comment, profile),
                                       is_user_minus_comment(comment, profile)])

    return all_likes_for_comments
