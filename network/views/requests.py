from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMultiAlternatives
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.shortcuts import render, redirect
from django.template.loader import get_template

from network.helpers.validation import is_profile_can_manage_school
from network.models import Profile, Teacher, School, RequestToSchool, SchoolAndStudent, Student, RequestFromParent, \
    StudentToParent
from network.models.administration import Administration
from network.models.parent import Parent, ParentAndSchool


def get_current_requests():
    """
    Возвращает список непроверенных заявок для подтверждения пользователя (на сайте от лица админа)
    :return: список заявок
    """
    all_unapproved_profiles = Profile.objects.all().filter(Q(is_role_confirm=None), ~Q(role="USER"))

    all_requests = list()
    for profile in all_unapproved_profiles:
        if profile.role == "PARENT":
            all_requests.extend(Parent.objects.all().filter(profile=profile))
        elif profile.role == "TEACHER":
            all_requests.extend(Teacher.objects.all().filter(profile=profile))
        elif profile.role == "ADMINISTRATION":
            all_requests.extend(Administration.objects.all().filter(profile=profile))
    return all_requests


@login_required
def all_requests_for_approve_users(request):
    """
    Страница подтверждения ролей пользователей
    :param request: запрос на страницу
    :return: html-страница
    """
    if request.user.is_superuser is False:
        return redirect('error_403')

    profile = Profile.objects.get(user=request.user)
    all_requests = get_current_requests()

    page = request.GET.get('page', 1)
    paginator = Paginator(all_requests, 16)

    try:
        all_requests = paginator.page(page)
    except PageNotAnInteger:
        all_requests = paginator.page(1)
    except EmptyPage:
        all_requests = paginator.page(paginator.num_pages)

    context = {
        'profile': profile,
        'all_requests': all_requests
    }

    return render(request, 'requests/all_requests_for_users.html', context)


@login_required
def approve_request_for_user(request, profile_id):
    """
    Подтверждение роли определенного пользователя
    :param request: запрос на страницу
    :param profile_id: id-профиля, роль которого надо подтвердить
    :return: html-страница
    """
    if request.user.is_superuser is False:
        return redirect('error_403')

    profile = Profile.objects.get(id=profile_id)

    profile.is_role_confirm = True
    profile.save()

    # Отправка почты
    d = {
        'first_name': profile.user.first_name,
    }
    plaintext = get_template('email/approve_request.txt').render(d)
    htmly = get_template('email/approve_request.html').render(d)

    subject, from_email, to = 'Социальная сеть для школ и учеников', 'course.work.bpi196.network@gmail.com', \
                              profile.user.email
    text_content = plaintext
    html_content = htmly
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

    all_requests = get_current_requests()

    page = request.GET.get('page', 1)
    paginator = Paginator(all_requests, 16)

    try:
        all_requests = paginator.page(page)
    except PageNotAnInteger:
        all_requests = paginator.page(1)
    except EmptyPage:
        all_requests = paginator.page(paginator.num_pages)

    context = {
        'profile': profile,
        'all_requests': all_requests,
        'success_text': "Заявка успешно одобрена!"
    }
    return render(request, 'requests/all_requests_for_users.html', context)


@login_required
def reject_request_for_user(request, profile_id):
    """
    Отклонение запроса роли определенного пользователя
    :param request: запрос на страницу
    :param profile_id: id-профиля, роль которого надо подтвердить
    :return: html-страница
    """
    if request.user.is_superuser is False:
        return redirect('error_403')

    profile = Profile.objects.get(id=profile_id)

    if profile.role == "TEACHER":
        teacher = Teacher.objects.get(profile=profile)
        teacher.delete()
    elif profile.role == "PARENT":
        parent = Parent.objects.get(profile=profile)
        parent.delete()

    profile.is_role_confirm = None
    profile.role = "USER"
    profile.save()

    # Отправка почты
    d = {
        'first_name': profile.user.first_name,
    }
    plaintext = get_template('email/reject_request.txt').render(d)
    htmly = get_template('email/reject_request.html').render(d)

    subject, from_email, to = 'Социальная сеть для школ и учеников', 'course.work.bpi196.network@gmail.com', \
                              profile.user.email
    text_content = plaintext
    html_content = htmly
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

    all_requests = get_current_requests()

    page = request.GET.get('page', 1)
    paginator = Paginator(all_requests, 16)

    try:
        all_requests = paginator.page(page)
    except PageNotAnInteger:
        all_requests = paginator.page(1)
    except EmptyPage:
        all_requests = paginator.page(paginator.num_pages)

    context = {
        'profile': profile,
        'all_requests': all_requests,
        'success_text': "Заявка успешно отклонена!"
    }
    return render(request, 'requests/all_requests_for_users.html', context)


@login_required
def all_requests_for_approve_school(request):
    """
    Страница подтверждения школ
    :param request: запрос на страницу
    :return: html-страница
    """
    if request.user.is_superuser is False:
        return redirect('error_403')

    profile = Profile.objects.get(user=request.user)
    all_requests = School.objects.all().filter(is_approve=False)

    page = request.GET.get('page', 1)
    paginator = Paginator(all_requests, 12)

    try:
        all_requests = paginator.page(page)
    except PageNotAnInteger:
        all_requests = paginator.page(1)
    except EmptyPage:
        all_requests = paginator.page(paginator.num_pages)

    context = {
        'profile': profile,
        'all_requests': all_requests
    }

    return render(request, 'requests/all_requests_for_schools.html', context)


@login_required
def approve_request_for_school(request, school_id):
    """
    Функция подтверждения определенной школы
    :param request: запрос на страницу
    :param school_id: id-школы для подтверждения
    :return: html-страница
    """
    if request.user.is_superuser is False:
        return redirect('error_403')

    school = School.objects.get(id=school_id)

    school.is_approve = True
    school.save()

    profile = Profile.objects.get(user=request.user)
    all_requests = School.objects.all().filter(is_approve=False)

    # Отправка почты
    d = {
        'first_name': school.creator.user.first_name,
        'school_name': school.short_name
    }
    plaintext = get_template('email/approve_request_for_school.txt').render(d)
    htmly = get_template('email/approve_request_for_school.html').render(d)

    subject, from_email, to = 'Социальная сеть для школ и учеников', 'course.work.bpi196.network@gmail.com', \
                              school.creator.user.email
    text_content = plaintext
    html_content = htmly
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

    page = request.GET.get('page', 1)
    paginator = Paginator(all_requests, 12)

    try:
        all_requests = paginator.page(page)
    except PageNotAnInteger:
        all_requests = paginator.page(1)
    except EmptyPage:
        all_requests = paginator.page(paginator.num_pages)

    context = {
        'profile': profile,
        'all_requests': all_requests,
        'success_text': "Заявка школы успешно одобрена!"
    }
    return render(request, 'requests/all_requests_for_schools.html', context)


@login_required
def reject_request_for_school(request, school_id):
    """
    Функция отклонения определенной школы
    :param request: запрос на страницу
    :param school_id: id-школы для подтверждения
    :return: html-страница
    """
    if request.user.is_superuser is False:
        return redirect('error_403')

    school = School.objects.get(id=school_id)
    school.delete()

    profile = Profile.objects.get(user=request.user)
    all_requests = School.objects.all().filter(is_approve=False)


    # Отправка почты
    d = {
        'first_name': school.creator.user.first_name,
        'school_name': school.short_name
    }
    plaintext = get_template('email/reject_request_for_school.txt').render(d)
    htmly = get_template('email/reject_request_for_school.html').render(d)

    subject, from_email, to = 'Социальная сеть для школ и учеников', 'course.work.bpi196.network@gmail.com', \
                              school.creator.user.email
    text_content = plaintext
    html_content = htmly
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

    page = request.GET.get('page', 1)
    paginator = Paginator(all_requests, 12)

    try:
        all_requests = paginator.page(page)
    except PageNotAnInteger:
        all_requests = paginator.page(1)
    except EmptyPage:
        all_requests = paginator.page(paginator.num_pages)

    context = {
        'profile': profile,
        'all_requests': all_requests,
        'success_text': "Заявка школы успешно отклонена, школа удалена"
    }
    return render(request, 'requests/all_requests_for_users.html', context)


@login_required
def requests_for_joining_school(request, school_id):
    """
    Страница заявок на присоединение к школе
    :param request: запрос на страницу
    :param school_id: id-школы, к которой хотят присоединиться
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    school = School.objects.get(id=school_id)

    # Проверка на то, что пользователь является администратором школы
    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    context = {
        'requests': RequestToSchool.objects.all().filter(school=school),
        'school': school
    }

    return render(request, 'school/join_school/requests_for_joining_school.html', context)


@login_required
def approve_request_for_join_school(request, school_id, request_id):
    """
    Страница одобрения заявок на присоединение к школе
    :param request: запрос на страницу
    :param school_id: id-школы, к которой хотят присоединиться
    :param request_id: id-заявки, которую хотят одобрить
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    school = School.objects.get(id=school_id)

    # Проверка на то, что пользователь является администратором школы
    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    current_request = RequestToSchool.objects.get(id=request_id)

    context = {
        'requests': RequestToSchool.objects.all().filter(school=school),
        'school': school
    }

    if current_request.profile.role == 'TEACHER':
        teacher = Teacher.objects.get(profile=current_request.profile)
        teacher.school = current_request.school
        teacher.save()
    elif current_request.profile.role == "STUDENT":
        student = Student.objects.get(profile=current_request.profile)

        if SchoolAndStudent.objects.filter(Q(student=student), Q(school=current_request.school)).exists():
            context['error_text'] = 'Этот ученик уже состоит в Вашей школе'
            return render(request, 'school/join_school/requests_for_joining_school.html', context)

        # Проверка на возможность перехода из одной школы в другую.
        # Если ученика одобряют в новую школу, то он больше не имеет доступ к старой
        if RequestToSchool.objects.filter(Q(student=student), Q(status="APPROVE")).exists():
            all_old_requests = RequestToSchool.objects.all().filter(Q(student=student), Q(status="APPROVE"))
            for req in all_old_requests:
                req.status = "REJECT"
                req.save()

            all_connections_with_old_school = SchoolAndStudent.objects.all().filter(student=student)
            for obj in all_connections_with_old_school:
                obj.delete()

        new_school_for_student = SchoolAndStudent(student=student, school=current_request.school)
        new_school_for_student.save()

        # Иногда родители могут привязаться к ребенку раньше, чем ребенок к школе. Здесь это надо проверить и добавить,
        # если такой записи не существует (+ родителей может быть несколько, отсюда цикл)
        if StudentToParent.objects.filter(student=student).exists():
            parents_of_student = StudentToParent.objects.all().filter(student=student)
            for parent in parents_of_student:
                parent_in_school = ParentAndSchool(parent=parent, school=school)
                parent_in_school.save()

    current_request.status = "APPROVE"
    current_request.save()

    context['success_text'] = "Заявка успешно одобрена!"
    return render(request, 'school/join_school/requests_for_joining_school.html', context)


@login_required
def reject_request_for_join_school(request, school_id, request_id):
    """
    Страница отклонения заявок на присоединение к школе
    :param request: запрос на страницу
    :param school_id: id-школы, к которой хотят присоединиться
    :param request_id: id-заявки, которую хотят отклонить
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    school = School.objects.get(id=school_id)

    # Проверка на то, что пользователь является администратором школы
    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    current_request = RequestToSchool.objects.get(id=request_id)
    current_request.status = "REJECT"
    current_request.save()

    # Проверка на то, что эта заявка уже была одобрена до этого - тогда надо почистить записи и учителя, и ученика
    if current_request.profile.role == "TEACHER" and Teacher.objects.get(
            profile=current_request.profile).school is not None:
        teacher = Teacher.objects.get(profile=current_request.profile)
        teacher.school = None
        teacher.save()
    elif current_request.profile.role == "STUDENT" and SchoolAndStudent.objects.filter(
            Q(student=Student.objects.get(profile=current_request.profile)), Q(school=current_request.school)).exists():
        student = Student.objects.get(profile=current_request.profile)
        student_in_school = SchoolAndStudent.objects.get(
            Q(student=student), Q(school=current_request.school))
        student_in_school.delete()

        # Если у ученика отзывают школу, то надо ее отозвать и у всех родителей
        if StudentToParent.objects.filter(student=student).exists():
            parents_of_student = StudentToParent.objects.all().filter(student=student)
            for parent in parents_of_student:
                parent_in_school = ParentAndSchool.objects.get(Q(school=school), Q(parent=parent))
                parent_in_school.delete()

    context = {
        'requests': RequestToSchool.objects.all().filter(school=school),
        'school': school,
        'success_text': "Заявка успешно отклонена!"
    }
    return render(request, 'school/join_school/requests_for_joining_school.html', context)


@login_required
def approve_parent_from_child(request, request_id):
    """
    Функция одобрения связи ребенок-родитель со стороны ребенка
    :param request: запрос на страницу
    :param request_id: id-заявки, которую хотят одобрить
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    request_from_parent = RequestFromParent.objects.get(id=request_id)

    if profile.role != "STUDENT" or request_from_parent.student.profile != profile:
        return redirect('error_403')

    if request_from_parent.status == "APPROVE":
        return redirect('error_403')

    request_from_parent.status = "APPROVE"
    request_from_parent.save()

    new_connection = StudentToParent(student=request_from_parent.student, parent=request_from_parent.parent)
    new_connection.save()

    if SchoolAndStudent.objects.filter(student=request_from_parent.student).exists():
        parent_in_school = ParentAndSchool(parent=request_from_parent.parent, school=SchoolAndStudent.objects.get(
            student=request_from_parent.student).school)
        parent_in_school.save()
    return redirect('account')


@login_required
def reject_parent_from_child(request, request_id):
    """
    Функция отклонения связи ребенок-родитель со стороны ребенка
    :param request: запрос на страницу
    :param request_id: id-заявки, которую хотят отклонить
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    request_from_parent = RequestFromParent.objects.get(id=request_id)

    if profile.role != "STUDENT" or request_from_parent.student.profile != profile:
        return redirect('error_403')

    if request_from_parent.status == "REJECT":
        return redirect('error_403')

    request_from_parent.status = "REJECT"
    request_from_parent.save()

    if StudentToParent.objects.filter(Q(student=request_from_parent.student),
                                      Q(parent=request_from_parent.parent)).exists():
        old_connection = StudentToParent.objects.get(Q(student=request_from_parent.student),
                                                     Q(parent=request_from_parent.parent))
        old_connection.delete()

        # Удаляем принадлежность родителя к школе
        old_connection_with_school = ParentAndSchool.objects.get(parent=request_from_parent.parent)
        old_connection_with_school.delete()
    return redirect('account')
