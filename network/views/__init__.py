from network.views.main_pages import *
from network.views.signup import *
from network.views.account import *
from network.views.error import *
from network.views.requests import *
from network.views.school import *
from network.views.blog import *
from network.views.gallery import *
