from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from network.helpers.validation import is_value_empty, is_profile_can_manage_school

from network.models import Profile, School, SchoolManage, Post, Folder, ImageForFolder


@login_required
def add_folder(request, school_id):
    """
    Добавление новой вкладки в галерею
    :param request: запрос на страницу
    :param school_id: id-школы
    :return: html-страница
    """
    school = School.objects.get(id=school_id)
    profile = Profile.objects.get(user=request.user)

    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    context = {
        'school': school,
        'profile': profile
    }

    if request.method == "POST":
        title = request.POST.get('title')
        cover = request.FILES.get('photo')
        gallery_photos = request.FILES.getlist('gallery_photo_add')

        if is_value_empty(title) or is_value_empty(cover) or len(gallery_photos) == 0:
            context['error_text'] = "Заполните все поля!"
            return render(request, 'school/gallery/add_folder.html', context)

        new_folder = Folder(title=title, cover=cover, author=profile, school=school)
        new_folder.save()

        add_cover_to_gallery = ImageForFolder(folder=new_folder, image=cover)
        add_cover_to_gallery.save()

        for image in gallery_photos:
            new_photo = ImageForFolder(folder=new_folder, image=image)
            new_photo.save()

        context['success_text'] = "Новая вкладка успешно добавлена!"
        return render(request, 'school/gallery/add_folder.html', context)
    return render(request, 'school/gallery/add_folder.html', context)


def each_folder(request, school_id, folder_id):
    """
    Функция, которая отображает каждую вкладку на отдельной странице
    :param request: запрос на страницу
    :param school_id: id-школы, чья галерея
    :param folder_id: id-вкладки, где галерея
    :return: html-страница
    """
    folder = Folder.objects.get(id=folder_id)
    school = School.objects.get(id=school_id)

    context = {
        'school': school,
        'folder': folder,
        'all_images': ImageForFolder.objects.all().filter(folder=folder)
    }

    return render(request, 'school/gallery/each_folder.html', context)
