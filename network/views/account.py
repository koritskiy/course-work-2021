import random
import string

from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render, redirect
from django.template.loader import get_template

from network.models import Student, CommentForTeacher
from network.models import Profile, SchoolClass, SubjectAndStudent, Teacher, TeacherAndSubject, School, RequestToSchool, \
    SchoolAndStudent, StudentToParent, RequestFromParent, User
from django.core.mail import send_mail, EmailMultiAlternatives
from network.helpers.validation import is_value_empty, is_email_unique
from network.models.administration import Administration
from network.models.parent import Parent


def get_correct_subjects(profile):
    """
    Возвращает любимые предметы для студентов и профильные для учителей
    :param profile: профиль студента/учителя
    :return: список предметов
    """
    if profile.role == "STUDENT":
        return SubjectAndStudent.objects.all().filter(profile=profile)
    elif profile.role == "TEACHER":
        return TeacherAndSubject.objects.all().filter(teacher=profile)


def get_parents_of_student(student):
    """
    Функция, которая возвращает родителей для студентов
    :param student: студент для проверки
    :return: список родителей
    """
    return StudentToParent.objects.all().filter(student=student)


def get_school_of_student(student):
    """

    :param student:
    :return:
    """
    school = Student.objects.get(id=student.id).school

    if SchoolAndStudent.objects.filter(student=student).exists():
        school = SchoolAndStudent.objects.get(student=student).school
        return True, school

    return False, school


def get_list_of_children_with_info():
    """
    Функция, которая возвращает список детей с дополнительной информацией
    :return: список детей с дополнительной информацией
    """
    students = Student.objects.all()

    all_children_with_info = list()
    for student in students:
        is_school_with_page, school = get_school_of_student(student)

        all_children_with_info.append(
            # Вложенный список вида: ученик, его родители, школьный класс, присоединен ли к школе на сайте, школа
            [student, get_parents_of_student(student), SchoolClass.objects.get(profile=student.profile),
             is_school_with_page, school]
        )
    return all_children_with_info


def get_subjects_for_teacher_with_info(profile):
    """
    Функция для вычисления выбранных и невыбранных предметов учителем
    :param profile: профиль учителя
    :return: список вида [предмет, выбран ли (True/False)]
    """
    teachers_choose_subjects = TeacherAndSubject.objects.all().filter(teacher=profile)
    teachers_subjects = list()
    for obj in teachers_choose_subjects:
        teachers_subjects.append(obj.subject)

    subjects = ["Математика", "Русский язык", "Чтение", "Труд", "Окружающий мир", "Музыка", "ИЗО", "Физкультура",
                "Родной язык", "Основы религиозных культур и светской этики", "Иностранный язык", "История",
                "Литература", "ОБЖ", "Технология", "География", "Биология", "Информатика", "Обществознание", "Черчение",
                "Физика", "Химия", "Экономика", "Правоведение", "Философия", "Астрономия"]

    subjects_with_info = list()
    for subject in subjects:
        if subject in teachers_subjects:
            subjects_with_info.append([subject, True])
        else:
            subjects_with_info.append([subject, False])

    return subjects_with_info


def get_subjects_for_student_with_info(profile):
    """
    Функция для вычисления выбранных и невыбранных предметов учеником
    :param profile: профиль ученика
    :return: список вида [предмет, выбран ли (True/False)]
    """
    students_choose_subjects = SubjectAndStudent.objects.all().filter(profile=profile)
    students_subjects = list()
    for obj in students_choose_subjects:
        students_subjects.append(obj.subject)

    subjects = ["Математика", "Русский язык", "Чтение", "Труд", "Окружающий мир", "Музыка", "ИЗО", "Физкультура",
                "Родной язык", "Основы религиозных культур и светской этики", "Иностранный язык", "История",
                "Литература", "ОБЖ", "Технология", "География", "Биология", "Информатика", "Обществознание", "Черчение",
                "Физика", "Химия", "Экономика", "Правоведение", "Философия", "Астрономия"]

    subjects_with_info = list()
    for subject in subjects:
        if subject in students_subjects:
            subjects_with_info.append([subject, True])
        else:
            subjects_with_info.append([subject, False])

    return subjects_with_info


@login_required
def account(request):
    """
    Профиль пользователя/админа
    :param request: запрос на страницу
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    context = {
        'profile': profile,
        'subjects': get_correct_subjects(profile),
        'request_for_joining_school': RequestToSchool.objects.all().filter(profile=profile)
    }

    if profile.role == "STUDENT":
        student = Student.objects.get(profile=profile)
        context['student'] = student

        # Если у ученика есть одобренная заявка в школу, то он ее увидит в личном кабинете
        if RequestToSchool.objects.filter(Q(student=student), Q(status="APPROVE")).exists():
            context['school_in_system'] = RequestToSchool.objects.get(Q(student=student), Q(status="APPROVE")).school

        # Если у ученика есть заявка от родителя - он ее увидит в личном кабинете
        if RequestFromParent.objects.filter(student=student).exists():
            context['count_requests_from_parent_in_review'] = RequestFromParent.objects.all(). \
                filter(Q(student=student), Q(status="REVIEW")).count()
            context['request_from_parent'] = RequestFromParent.objects.all().filter(student=student)
    elif profile.role == "PARENT":
        parent = Parent.objects.get(profile=profile)
        context['parent'] = parent

        # Если родитель присоединил к себе ребенка - нужно, чтобы он смог смотреть на статус заявки
        if RequestFromParent.objects.filter(parent=parent).exists():
            context['requests_to_children'] = RequestFromParent.objects.all().filter(parent=parent)

        # Если у родителя есть подтвержденные дети, то надо их отобразить
        if StudentToParent.objects.filter(parent=parent).exists():
            context['children'] = StudentToParent.objects.all().filter(parent=parent)
    elif profile.role == "TEACHER":
        context['teacher'] = Teacher.objects.get(profile=profile)
    elif profile.role == "ADMINISTRATION":
        context['administration'] = Administration.objects.get(profile=profile)
        context['schools'] = School.objects.all().filter(creator=profile)

    return render(request, 'account/account.html', context)


@login_required
def get_info_about_student(request):
    """
    Страница для получения информации об ученике
    :param request: запрос на страницу
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    context = {
        'profile': profile
    }

    if request.method == "POST":
        city = request.POST.get('city')
        school = request.POST.get('school')
        student_class = request.POST.get('class')
        letter_class = request.POST.get('letter')
        subject = request.POST.getlist('subject')

        if is_value_empty(city) or is_value_empty(school) or is_value_empty(
                letter_class) or subject == [] or letter_class == 0 or student_class == 0:
            context['error_text'] = "Заполните все поля!"
            return render(request, 'account/choose_role/student.html', context)

        school_class = SchoolClass(profile=profile, number=student_class, letter=letter_class)
        school_class.save()

        student = Student(profile=profile, city=city, school=school, school_class=school_class)
        student.save()

        for i in subject:
            new_favourite_subject = SubjectAndStudent(subject=i, profile=profile)
            new_favourite_subject.save()

        profile.role = "STUDENT"
        profile.is_role_confirm = True
        profile.save()

        context['success_text'] = "Информация успешно загружена!"
        return render(request, 'account/choose_role/student.html', context)
    return render(request, 'account/choose_role/student.html', context)


@login_required
def get_info_about_parent(request):
    """
    Страница для получения информации о родителе
    :param request: запрос на страницу
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    context = {
        'profile': profile
    }

    if request.method == "POST":
        city = request.POST.get('city')

        parent = Parent(profile=profile, city=city)
        parent.photo = request.FILES.get('photo')
        parent.save()

        profile.role = "PARENT"
        profile.save()

        context['success_text'] = "Информация успешно отправлена!"
        return render(request, 'account/choose_role/parent.html', context)
    return render(request, 'account/choose_role/parent.html', context)


@login_required
def get_info_about_teacher(request):
    """
    Страница получения информации об учителе
    :param request: запрос на страницу
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    context = {
        'profile': profile
    }

    if request.method == "POST":
        city = request.POST.get('city')
        middle_name = request.POST.get('middle_name')
        subject = request.POST.getlist('subject')

        teacher = Teacher(profile=profile, middle_name=middle_name, city=city)
        teacher.photo = request.FILES.get('photo')
        teacher.save()

        for i in subject:
            new_subject = TeacherAndSubject(subject=i, teacher=profile)
            new_subject.save()

        profile.role = "TEACHER"
        profile.save()
        context['success_text'] = "Информация успешно отправлена!"
        return render(request, 'account/choose_role/teacher.html', context)
    return render(request, 'account/choose_role/teacher.html', context)


@login_required
def get_info_about_administration(request):
    """
        Страница получения информации об учителе
        :param request: запрос на страницу
        :return: html-страница
        """
    profile = Profile.objects.get(user=request.user)
    context = {
        'profile': profile
    }

    if request.method == "POST":
        city = request.POST.get('city')
        middle_name = request.POST.get('middle_name')
        position = request.POST.getlist('position')

        administration = Administration(profile=profile, middle_name=middle_name, city=city, position=position)
        administration.photo = request.FILES.get('photo')
        administration.save()

        profile.role = "ADMINISTRATION"
        profile.save()
        context['success_text'] = "Информация успешно отправлена!"
        return render(request, 'account/choose_role/administration.html', context)
    return render(request, 'account/choose_role/administration.html', context)


@login_required
def choose_child(request):
    """
    Страница выбора ребенка для его привязке к родителю
    :param request: запрос на страницу
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    if profile.role != "PARENT" or profile.is_role_confirm is False:
        return redirect('error_403')

    context = {
        'children': get_list_of_children_with_info()
    }
    return render(request, 'account/join_child/choose_child.html', context)


@login_required
def request_to_child(request, profile_id):
    """
    Функция для отправки заявки присоединения ребенка к родителю
    :param request: запрос на страницу
    :param profile_id: профиль выбранного ученика
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    if profile.role != "PARENT" or profile.is_role_confirm is False:
        return redirect('error_403')

    parent = Parent.objects.get(profile=profile)
    student_profile = Profile.objects.get(id=profile_id)
    student = Student.objects.get(profile=student_profile)

    context = {
        'children': get_list_of_children_with_info()
    }

    if RequestFromParent.objects.filter(Q(student=student), Q(parent=parent), ~Q(status="REJECT")).exists():
        context['error_text'] = "Этот ребенок уже добавлен к Вам в профиль или заявка находится в рассмотрении!"
        return render(request, 'account/join_child/choose_child.html', context)

    new_request_to_child = RequestFromParent(student=student, parent=parent)
    new_request_to_child.save()

    context['success_text'] = "Заявка успешно отправлена! Теперь надо подождать ответа от ребенка"
    return render(request, 'account/join_child/choose_child.html', context)


@login_required
def edit_avatar(request):
    profile = Profile.objects.get(user=request.user)
    if request.method == "POST":
        profile.photo = request.FILES['avatar']
        profile.save()
        return redirect('account')
    return render(request, 'account/settings/edit_image.html', {'profile': profile})


@login_required
def edit_email(request):
    profile = Profile.objects.get(user=request.user)

    context = {
        'profile': profile
    }

    if request.method == "POST":
        new_email = request.POST.get('email')

        if is_email_unique(new_email) is False:
            context['error_text'] = 'Пользователь с такой почтой уже существует. Попробуйте еще раз!'
            return render(request, 'account/settings/edit_email.html', context)
        profile.user.email = new_email
        profile.user.save()

        profile.is_email_confirm = False
        profile.code = random.randint(100000, 999999)
        profile.save()

        # Отправка почты
        d = {
            'first_name': profile.user.first_name,
            'code': profile.code
        }
        plaintext = get_template('email/signup.txt').render(d)
        htmly = get_template('email/signup.html').render(d)

        subject, from_email, to = 'Социальная сеть для школ и учеников', 'course.work.bpi196.network@gmail.com', \
                                  profile.user.email
        text_content = plaintext
        html_content = htmly
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

        context['success_text'] = 'Почта успешно изменена!'
        context['profile'] = Profile.objects.get(user=request.user)
        return render(request, 'account/settings/edit_email.html', context)
    return render(request, 'account/settings/edit_email.html', context)


@login_required
def edit_password(request):
    profile = Profile.objects.get(user=request.user)
    context = {
        'profile': profile
    }
    if request.method == "POST":
        old_password = request.POST.get('old_password')
        new_password = request.POST.get('password1')
        new_password_confirm = request.POST.get('password2')

        if request.user.check_password(old_password) is False:
            context['error_text'] = "Вы ввели неправильный текущий пароль, попробуйте еще раз"
            return render(request, 'account/settings/edit_password.html', context)

        if new_password != new_password_confirm:
            context['error_text'] = 'Новые пароли не совпали, попробуйте еще раз'
            return render(request, 'account/settings/edit_password.html', context)

        request.user.set_password(new_password)
        request.user.save()
        update_session_auth_hash(request, request.user)

        context['success_text'] = 'Пароль успешно изменен!'
        return render(request, 'account/settings/edit_password.html', context)
    return render(request, 'account/settings/edit_password.html', context)


@login_required
def confirm_email(request):
    profile = Profile.objects.get(user=request.user)
    context = {
        'profile': profile
    }

    if request.method == "POST" and 'submit_general_email_confirm_form' in request.POST:
        # Подтверждение основной почты
        code = request.POST.get('general_email_code')

        if int(code) != profile.code:
            context['error_text'] = "Код неверен, попробуйте еще раз"
            return render(request, 'account/settings/confirm_email.html', context)

        profile.is_email_confirm = True
        profile.save()
        context['success_text'] = 'Основная почта успешно подтверждена!'
        return render(request, 'account/settings/confirm_email.html', context)
    return render(request, 'account/settings/confirm_email.html', context)


@login_required
def edit_student(request):
    profile = Profile.objects.get(user=request.user)
    if profile.role != "STUDENT":
        return redirect('error_403')

    student = Student.objects.get(profile=profile)

    context = {
        'profile': profile,
        'student': student,
        'subjects': get_subjects_for_student_with_info(profile)
    }

    if request.method == "POST":
        name = request.POST.get('name')
        surname = request.POST.get('surname')
        subjects = request.POST.getlist('subject')

        profile.user.first_name = name
        profile.user.last_name = surname
        profile.user.save()

        for subject in subjects:
            if SubjectAndStudent.objects.filter(Q(profile=profile), Q(subject=subject)).exists() is False:
                new_subject_for_student = SubjectAndStudent(profile=profile, subject=subject)
                new_subject_for_student.save()

        context['success_text'] = "Данные успешно изменены!"
        return render(request, 'account/settings/edit_student.html', context)
    return render(request, 'account/settings/edit_student.html', context)


@login_required
def edit_teacher(request):
    profile = Profile.objects.get(user=request.user)
    if profile.role != "TEACHER":
        return redirect('error_403')

    teacher = Teacher.objects.get(profile=profile)

    context = {
        'profile': profile,
        'teacher': teacher,
        'subjects': get_subjects_for_teacher_with_info(profile)
    }

    if request.method == "POST":
        name = request.POST.get('name')
        middle_name = request.POST.get('middle_name')
        surname = request.POST.get('surname')
        subjects = request.POST.getlist('subject')

        profile.user.first_name = name
        profile.user.last_name = surname
        profile.user.save()

        teacher.middle_name = middle_name
        teacher.save()

        for subject in subjects:
            if TeacherAndSubject.objects.filter(Q(teacher=profile), Q(subject=subject)).exists() is False:
                new_subject_for_teacher = TeacherAndSubject(teacher=profile, subject=subject)
                new_subject_for_teacher.save()

        context['success_text'] = "Данные успешно изменены!"
        return render(request, 'account/settings/edit_teacher.html', context)
    return render(request, 'account/settings/edit_teacher.html', context)


@login_required
def each_user(request, user_id):
    user = User.objects.get(id=user_id)
    if user == request.user:
        return redirect("account")

    profile = Profile.objects.get(user=user)
    context = {
        'profile': profile,
        'request_profile': Profile.objects.get(user=request.user)
    }

    if Teacher.objects.filter(profile=profile).exists():
        teacher = Teacher.objects.get(profile=profile)
        context['comments'] = CommentForTeacher.objects.all().filter(teacher=teacher)

    if request.method == "POST":
        comment = request.POST.get('comment')

        if is_value_empty(comment):
            context['error_text'] = "Поле комментария не может быть пустым!"
            return render(request, 'account/each_user.html', context)

        teacher = Teacher.objects.get(profile=profile)
        author = Profile.objects.get(user=request.user)

        new_comment = CommentForTeacher(teacher=teacher, comment=comment, author=author)
        new_comment.save()

        context['success_text'] = "Комментарий успешно добавлен!"
        return render(request, 'account/each_user.html', context)
    return render(request, 'account/each_user.html', context)


@login_required
def send_email_code_again(request, email):
    profile = Profile.objects.get(user=request.user)

    # Отправка почты
    d = {
        'first_name': profile.user.first_name,
        'code': profile.code
    }
    plaintext = get_template('email/signup.txt').render(d)
    htmly = get_template('email/signup.html').render(d)

    subject, from_email, to = 'Социальная сеть для школ и учеников', 'course.work.bpi196.network@gmail.com', \
                              profile.user.email
    text_content = plaintext
    html_content = htmly
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()

    context = {
        'profile': profile,
        'success_text': 'Письмо повторно отправлено Вам на почту!'
    }
    return render(request, 'account/settings/confirm_email.html', context)


def forget_password(request):
    context = {

    }

    if request.method == "POST":
        email_for_new_password = request.POST.get('email_for_new_password')
        if is_value_empty(email_for_new_password):
            context['error_text'] = "Заполните все поля!"
            return render(request, 'account/settings/forget_password.html', context)

        try:
            user = User.objects.get(email=email_for_new_password)
        except User.DoesNotExist:
            user = False

        if user is False:
            context['error_text'] = "Пользователя с такой почтой не существует"
            return render(request, 'account/settings/forget_password.html', context)

        profile = Profile.objects.get(user=user)

        if profile.is_email_confirm is False:
            context['error_text'] = "Эта почта не подтверждена. Обратитесь к разработчикам."
            return render(request, 'account/settings/forget_password.html', context)

        new_password = ''.join(random.choice(string.ascii_lowercase) for i in range(8))
        user.set_password(new_password)
        user.save()

        d = {
            'first_name': profile.user.first_name,
            'password': new_password
        }
        plaintext = get_template('email/forget_password.txt').render(d)
        htmly = get_template('email/forget_password.html').render(d)

        subject, from_email, to = 'Социальная сеть для школ и учеников', 'course.work.bpi196.network@gmail.com', \
                                  profile.user.email
        text_content = plaintext
        html_content = htmly
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

        context['success_text'] = "Пароль успешно изменен! Новый пароль пришел к Вам на почту"
        return render(request, 'account/settings/forget_password.html', context)
    return render(request, 'account/settings/forget_password.html', context)
