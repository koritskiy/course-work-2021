from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render, redirect
from network.helpers.validation import is_value_empty, is_profile_can_manage_school
from network.helpers.likes import get_all_likes_for_posts, get_all_likes_for_comments
from network.models import Profile, School, SchoolManage, Post, LikeForPost, Comment, RateComment, Teacher, \
    SchoolAndStudent, Student
from network.models.parent import ParentAndSchool, Parent


def get_all_comments_with_info_about_author(school, post):
    """
    Возвращает список комментарией с дополнительной информацией
    список вида [комментарий, автор комментария участник школы или нет]
    :param school: школа для проверки
    :param post: пост для проверки
    :return: список комментарией с дополнительной информацией
    """
    comments = Comment.objects.all().filter(post=post)[::-1]

    all_comments_with_info = list()
    for comment in comments:
        # Ищем, является ли автор комментария участником школы
        if comment.author.role == "STUDENT" and SchoolAndStudent.objects.filter(Q(school=school), Q(
                student=Student.objects.get(profile=comment.author))).exists():
            all_comments_with_info.append([comment, True])
        elif comment.author.role == "TEACHER" and Teacher.objects.filter(Q(school=school),
                                                                         Q(profile=comment.author)).exists():
            all_comments_with_info.append([comment, True])
        elif comment.author.role == "PARENT" and ParentAndSchool.objects.filter(Q(school=school), Q(
                parent=Parent.objects.get(profile=comment.author))).exists():
            all_comments_with_info.append([comment, True])
        else:
            all_comments_with_info.append([comment, False])

    return all_comments_with_info


@login_required
def add_post(request, school_id):
    """
    Добавления нового поста в ленту школы
    :param request: запрос на страницу
    :param school_id: id-школы
    :return: html-страница
    """
    school = School.objects.get(id=school_id)
    profile = Profile.objects.get(user=request.user)

    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    context = {
        'school': school,
        'profile': profile
    }

    if request.method == "POST":
        title = request.POST.get('title')
        content = request.POST.get('content')
        photo = request.FILES.get('photo')

        if is_value_empty(title) or is_value_empty(content):
            context['error_text'] = "Заполните поля заголовка и контента для создания поста!"
            return render(request, 'school/blog/add_post.html', context)

        new_post = Post(title=title, content=content, photo=photo, author=profile, school=school)
        new_post.save()
        context['success_text'] = "Новость успешно добавлена!"
        return render(request, 'school/blog/add_post.html', context)

    return render(request, 'school/blog/add_post.html', context)


def each_post(request, school_id, post_id):
    """
    Страница каждого отдельного поста
    :param request: запрос на страницу
    :param school_id: id-школы
    :param post_id: id-поста
    :return: html-страница
    """
    school = School.objects.get(id=school_id)
    post = Post.objects.get(id=post_id)
    profile = Profile.objects.get(user=request.user)
    comments = Comment.objects.all().filter(post=post)[::-1]

    context = {
        'school': school,
        'post': post,
        'likes': get_all_likes_for_posts(Post.objects.all().filter(id=post_id), profile),
        'comments': get_all_comments_with_info_about_author(school, post),
        'comment_votes': get_all_likes_for_comments(comments, profile),
        'profile': profile
    }

    if request.method == "POST":
        comment = request.POST.get('comment')

        if profile.is_role_confirm is False:
            context['error_text'] = "Для написания комментария нужно подтвердить профиль!"
            return render(request, 'school/blog/each_post.html', context)

        if is_value_empty(comment):
            context['error_text'] = "Заполните поле комментария!"
            return render(request, 'school/blog/each_post.html', context)

        comment = Comment(content=comment, author=profile, post=post)
        comment.save()
        context['success_text'] = "Комментарий успешно опубликован!"
        context['comments'] = get_all_comments_with_info_about_author(school, post)
        return render(request, 'school/blog/each_post.html', context)
    return render(request, 'school/blog/each_post.html', context)


@login_required
def like_for_post(request, school_id, post_id):
    """
    Новый лайк к посту в ленте школы
    :param request: запрос на страницу
    :param school_id: id-школы
    :param post_id: id-поста
    :return: html-страница
    """
    school = School.objects.get(id=school_id)
    post = Post.objects.get(id=post_id)
    profile = Profile.objects.get(user=request.user)

    if LikeForPost.objects.all().filter(Q(school=school), Q(post=post), Q(author=profile)).exists():
        return redirect('error_403')

    like = LikeForPost(school=school, post=post, author=profile)
    like.save()
    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def unlike_for_post(request, school_id, post_id):
    """
    Дизлайк к посту в ленте школы (если пользователь тыкнет на лайк второй раз - включится как раз эта функция)
    :param request: запрос на страницу
    :param school_id: id-школы
    :param post_id: id-поста
    :return: html-страница
    """
    school = School.objects.get(id=school_id)
    post = Post.objects.get(id=post_id)
    profile = Profile.objects.get(user=request.user)

    if LikeForPost.objects.all().filter(Q(school=school), Q(post=post), Q(author=profile)).exists() is False:
        return redirect('error_403')

    like = LikeForPost.objects.get(Q(school=school), Q(post=post), Q(author=profile))
    like.delete()
    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def plus_for_comment(request, comment_id):
    """
    Функция, которая ставит плюс в комментарий
    :param request: запрос на страницу
    :param comment_id: id-комментария
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    comment = Comment.objects.get(id=comment_id)

    if profile.role == "USER":
        return redirect('error_403')

    # Проверка на само-плюс
    if comment.author == profile:
        return redirect('error_403')

    # Проверка на то, что уже есть один плюс. Если есть, то второй раз не даем плюсануть
    if RateComment.objects.all().filter(Q(comment=comment), Q(is_plus=True), Q(author=profile)).exists():
        return redirect('error_403')

    # Проверка на то, что у пользователя был до этого минус. Если есть, то надо удалить минус и добавить плюс
    if RateComment.objects.all().filter(Q(comment=comment), Q(is_plus=False), Q(author=profile)).exists():
        old_minus_for_comment = RateComment.objects.all().filter(Q(comment=comment), Q(is_plus=False),
                                                                 Q(author=profile))
        old_minus_for_comment.delete()

        new_plus_for_comment = RateComment(comment=comment, author=profile, is_plus=True)
        new_plus_for_comment.save()
        return redirect(request.META.get('HTTP_REFERER'))

    new_plus_for_comment = RateComment(comment=comment, author=profile, is_plus=True)
    new_plus_for_comment.save()
    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def minus_for_comment(request, comment_id):
    """
    Функция, которая ставит минус в комментарий
    :param request: запрос на страницу
    :param comment_id: id-комментария
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    comment = Comment.objects.get(id=comment_id)

    if profile.role == "USER":
        return redirect('error_403')

    # Проверка на само-минус
    if comment.author == profile:
        return redirect('error_403')

    # Проверка на то, что уже есть один минус. Если есть, то второй раз не даем минусануть
    if RateComment.objects.all().filter(Q(comment=comment), Q(is_plus=False), Q(author=profile)).exists():
        return redirect('error_403')

    # Проверка на то, что у пользователя был до этого плюс. Если есть, то надо удалить плюс и добавить минус
    if RateComment.objects.all().filter(Q(comment=comment), Q(is_plus=True), Q(author=profile)).exists():
        old_plus_for_comment = RateComment.objects.all().filter(Q(comment=comment), Q(is_plus=True), Q(author=profile))
        old_plus_for_comment.delete()

        new_minus_for_comment = RateComment(comment=comment, author=profile, is_plus=False)
        new_minus_for_comment.save()
        return redirect(request.META.get('HTTP_REFERER'))

    new_minus_for_comment = RateComment(comment=comment, author=profile, is_plus=False)
    new_minus_for_comment.save()
    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def edit_post(request, school_id, post_id):
    school = School.objects.get(id=school_id)
    profile = Profile.objects.get(user=request.user)
    post = Post.objects.get(id=post_id)

    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    context = {
        'school': school,
        'profile': profile,
        'post': post
    }

    if request.method == "POST":
        title = request.POST.get('title')
        content = request.POST.get('content')

        if is_value_empty(title) or is_value_empty(content):
            context['error_text'] = "Заполните поля заголовка и контента для изменения поста!"
            return render(request, 'school/blog/edit_post.html', context)

        post.title = title
        post.content = content
        post.save()

        context['success_text'] = "Пост успешно изменен!"
        return render(request, 'school/blog/edit_post.html', context)
    return render(request, 'school/blog/edit_post.html', context)


@login_required
def delete_post(request, school_id, post_id):
    school = School.objects.get(id=school_id)
    profile = Profile.objects.get(user=request.user)
    post = Post.objects.get(id=post_id)

    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    post.delete()
    return redirect(request.META.get('HTTP_REFERER'))
