import json

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render, redirect
from network.helpers.validation import is_value_empty, is_profile_can_manage_school
from network.helpers.likes import get_all_likes_for_posts
from network.models import Profile, School, Post, Folder, LikeForPost, Teacher, Student, RequestToSchool, \
    TeacherAndSubject, SchoolManage, AverageMarksForExam, InfoAboutOlympics, UniversityYear, StatsByUniversityInOneYear
from network.models.administration import Administration


def get_people_for_school(school):
    """
    Функция, которая возвращает всех представителей школы (учителей и администрацию)
    :param school: школа для поиска учителей
    :return: список вида [учитель, профильные предметы учителя] или [администрация, должность]
    """
    teachers = Teacher.objects.all().filter(school=school)

    people_of_school = list()
    for teacher in teachers:
        people_of_school.append([teacher, TeacherAndSubject.objects.all().filter(teacher=teacher.profile)])

    administrations = Administration.objects.all().filter(school=school)
    for admin in administrations:
        people_of_school.append([admin, admin.position])
    return people_of_school


def get_candidates_for_admin_school(school):
    """
    Функция, которая возвращает кандидатов для администрирования школы (присоединенные к школе учителя)
    :param school: школа для проверки
    :return: список вида [учитель школы, является ли учитель админом этой школы]
    """
    teachers = Teacher.objects.all().filter(school=school)

    candidtes = list()
    # Возвращает список вида: учитель, является ли уже админом
    for teacher in teachers:
        is_already_admin = SchoolManage.objects.filter(Q(school=school), Q(profile=teacher.profile)).exists()
        candidtes.append([teacher, is_already_admin])
    return candidtes


@login_required
def create_school(request):
    """
    Страница создания школы
    :param request: запрос на страницу
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    if profile.role != "ADMINISTRATION" or profile.is_role_confirm is not True:
        return redirect('error_403')
    context = {
        'profile': profile
    }

    if request.method == "POST":
        short_name = request.POST.get('short_name')
        full_name = request.POST.get('full_name')
        country = request.POST.get('country')
        city = request.POST.get('city')
        address = request.POST.get('address')
        coords = json.loads(request.POST.get('coords'))
        number = request.POST.get('number')
        phone_coord = request.POST.get('phone_coord')
        phone_school = request.POST.get('phone_school')
        email_school = request.POST.get('email_school')

        if is_value_empty(short_name) or is_value_empty(full_name) or is_value_empty(address) or is_value_empty(
                number) or is_value_empty(phone_coord) or is_value_empty(phone_school) or is_value_empty(email_school):
            context['error_text'] = "Заполните все поля!"
            return render(request, 'school/add_school.html', context)

        school = School(short_name=short_name, full_name=full_name, number=number, address=address,
                        phone_coordinator=phone_coord, phone=phone_school, email=email_school, country=country,
                        city=city, coords_longitude=coords[0], coords_latitude=coords[1], creator=profile)
        school.save()

        administration = Administration.objects.get(profile=profile)
        administration.school = school
        administration.save()

        new_admin_for_school = SchoolManage(school=school, profile=profile)
        new_admin_for_school.save()

        context['success_text'] = "Школа успешно добавлена!"
        return render(request, 'school/add_school.html', context)
    return render(request, 'school/add_school.html', context)


def each_school(request, school_id):
    """
    Страница каждой школы
    :param request: запрос на страницу
    :param school_id: id-школы, чья будет страница
    :return: html-страница
    """
    school = School.objects.get(id=school_id)
    profile = Profile.objects.get(user=request.user)

    # Последние три поста (самые свежие в ленте школы)
    posts = Post.objects.all().filter(school=school)[::-1]
    if len(posts) > 3:
        posts = posts[:3]

    # Считаем лайки к каждому посту
    all_likes_for_posts = get_all_likes_for_posts(posts, profile)

    # Проверяем, есть ли информация по экзаменам ЕГЭ
    is_marks_for_exam_exist = False
    if len(AverageMarksForExam.objects.all().filter(school=school)) > 0:
        is_marks_for_exam_exist = True

    # Проверяем, есть ли информация об успехах на олимпиадах
    is_info_about_olympics_exist = False
    if len(InfoAboutOlympics.objects.all().filter(school=school)) > 0:
        is_info_about_olympics_exist = True

    # Проверяем, есть ли информация о статистике поступления
    is_info_about_university = False
    if len(StatsByUniversityInOneYear.objects.all().filter(school=school)) > 0:
        is_info_about_university = True

    context = {
        'school': school,
        'profile': profile,
        'can_manage': is_profile_can_manage_school(profile, school),
        'posts': posts,
        'folders': Folder.objects.all().filter(school=school),
        'like_for_posts': all_likes_for_posts,
        'all_likes': LikeForPost.objects.all().filter(school=school),
        'people_for_school': get_people_for_school(school),
        'is_marks_for_exam_exist': is_marks_for_exam_exist,
        'is_info_about_olympics_exist': is_info_about_olympics_exist,
        'is_info_about_university': is_info_about_university
    }

    if is_marks_for_exam_exist:
        context['average_marks'] = AverageMarksForExam.objects.all().filter(school=school)

    if is_info_about_olympics_exist:
        olympics = InfoAboutOlympics.objects.all().filter(school=school)[::-1]
        context['olympics'] = olympics[:3]
        context['is_olympics_more_than_three'] = len(olympics) > 3

    if is_info_about_university:
        context['years_for_university'] = UniversityYear.objects.all().filter(school=school)
        context['stats_for_year_of_university'] = StatsByUniversityInOneYear.objects.all().filter(school=school)

    return render(request, 'school/each_school.html', context)


def show_all_info_about_olympics(request, school_id):
    school = School.objects.get(id=school_id)
    olympics = InfoAboutOlympics.objects.all().filter(school=school)

    context = {
        'olympics': olympics[::-1],
        'school': school
    }
    return render(request, 'school/additional_information/show_all_info_about_olympic.html', context)


def all_posts_for_school(request, school_id):
    """
    Вся лента на одной странице (все посты для школы на одной странице)
    :param request: запрос на страницу
    :param school_id: id-школы, чью ленту будем отображать
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    school = School.objects.get(id=school_id)
    posts = Post.objects.all().filter(school=school)[::-1]
    context = {
        'school': school,
        'posts': posts,
        'profile': profile,
        'like_for_posts': get_all_likes_for_posts(posts, profile)
    }

    return render(request, 'school/blog/all_posts_for_school.html', context)


@login_required
def join_school(request):
    """
    Функция присоединения к школе (после подтверждения аккаунта пользователь может выбрать себе школу из существующих
    на сайте
    :param request: запрос на страницу
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    if profile.role == "USER":
        return redirect('error_403')

    context = {
        'profile': profile,
        'schools': School.objects.all()
    }

    if profile.role == "TEACHER":
        context['teacher'] = Teacher.objects.get(profile=profile)
    elif profile.role == "STUDENT":
        context['student'] = Student.objects.get(profile=profile)

    if request.method == "POST" and 'school_with_page' in request.POST:
        school_id = request.POST.get('school_id')

        if school_id == 0:
            context['error_text'] = "Выберите какую-нибудь школу!"
            return render(request, 'school/join_school/join_to_school.html', context)

        school = School.objects.get(id=school_id)
        if RequestToSchool.objects.filter(Q(school=school), Q(profile=profile)).exists():
            context['error_text'] = "Нельзя подать заявку второй раз в одну и ту же школу!"
            return render(request, 'school/join_school/join_to_school.html', context)

        if profile.role == "TEACHER":
            new_request_to_join_school = RequestToSchool(school=school, profile=profile,
                                                         teacher=Teacher.objects.get(profile=profile))
            new_request_to_join_school.save()
        elif profile.role == "STUDENT":
            new_request_to_join_school = RequestToSchool(school=school, profile=profile,
                                                         student=Student.objects.get(profile=profile))
            new_request_to_join_school.save()

        context['success_text'] = "Заявка успешно отправлена! Статус Вы можете увидеть в личном кабинете"
        return render(request, 'school/join_school/join_to_school.html', context)

    if request.method == "POST" and 'school_without_page' in request.POST:
        school = request.POST.get('school')

        if is_value_empty(school):
            context['error_text'] = "Заполните поле с школой!"
            return render(request, 'school/join_school/join_to_school.html', context)

        student = Student.objects.get(profile=profile)
        student.school = school
        student.save()

        context['success_text'] = "Школа успешно изменена!"
        context['student'] = student
        return render(request, 'school/join_school/join_to_school.html', context)
    return render(request, 'school/join_school/join_to_school.html', context)


def all_schools(request):
    """
    Страница всех школ
    :param request: запрос на страницу
    :return: html-страница
    """
    context = {
        'schools': School.objects.all()
    }
    return render(request, 'school/all_schools.html', context)


@login_required
def add_admin_for_school(request, school_id):
    """
    Страница добавления администратора для школы
    :param request: запрос на страницу
    :param school_id: id-школы, в которую хотят добавить админа
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    school = School.objects.get(id=school_id)

    # Проверка на то, что пользователь является администратором школы
    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    context = {
        'school': school,
        'candidates': get_candidates_for_admin_school(school)
    }

    return render(request, 'school/settings/add_admin.html', context)


@login_required
def make_admin_for_school(request, school_id, profile_id):
    """
    Функция, дающая права администратора для нового профиля в конкретной школе
    :param request: запрос на страницу
    :param school_id: id-школы, в которую будет добавлен администратор
    :param profile_id: id-профиля, который станет новым администратором
    :return: html-страница
    """
    profile = Profile.objects.get(user=request.user)
    school = School.objects.get(id=school_id)

    # Проверка на то, что пользователь является администратором школы
    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    context = {
        'school': school
    }

    new_admin = Profile.objects.get(id=profile_id)

    if SchoolManage.objects.filter(profile=new_admin).exists():
        context['error_text'] = "Этот пользователь уже является админом!"
        return render(request, 'school/settings/add_admin.html', context)

    new_admin_connection = SchoolManage(school=school, profile=new_admin)
    new_admin_connection.save()

    context['success_text'] = "Пользователь успешно добавлен в админы!"
    context['candidates'] = get_candidates_for_admin_school(school)
    return render(request, 'school/settings/add_admin.html', context)


@login_required
def edit_school(request, school_id):
    profile = Profile.objects.get(user=request.user)
    school = School.objects.get(id=school_id)

    # Проверка на то, что пользователь является администратором школы
    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    context = {
        'school': school
    }

    if request.method == "POST":
        short_name = request.POST.get('short_name')
        full_name = request.POST.get('full_name')
        country = request.POST.get('country')
        city = request.POST.get('city')
        address = request.POST.get('address')
        coords = json.loads(request.POST.get('coords'))
        number = request.POST.get('number')
        phone_coord = request.POST.get('phone_coord')
        phone_school = request.POST.get('phone_school')
        email_school = request.POST.get('email_school')

        if is_value_empty(short_name) or is_value_empty(full_name) or is_value_empty(address) or is_value_empty(
                number) or is_value_empty(phone_coord) or is_value_empty(phone_school) or is_value_empty(email_school):
            context['error_text'] = "Заполните все поля!"
            return render(request, 'school/add_school.html', context)

        school.short_name = short_name
        school.full_name = full_name
        school.country = country
        school.city = city
        school.address = address
        school.coords_longitude = coords[0]
        school.coords_latitude = coords[1]
        school.number = number
        school.phone_coordinator = phone_coord
        school.phone = phone_school
        school.email = email_school
        school.save()

        context['success_text'] = "Школа успешно изменена!"
        return render(request, 'school/settings/edit_school.html', context)
    return render(request, 'school/settings/edit_school.html', context)


@login_required
def add_additional_info_for_school(request, school_id):
    profile = Profile.objects.get(user=request.user)
    school = School.objects.get(id=school_id)

    # Проверка на то, что пользователь является администратором школы
    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    context = {
        'school': school
    }

    if request.method == "POST":
        place_in_rating = request.POST.get('place_in_rating')

        if is_value_empty(place_in_rating):
            context['error_text'] = "Заполните все поля!"
            return render(request, 'school/additional_information/main.html', context)

        school.place_in_rating = place_in_rating
        school.save()
        context['success_text'] = "Место в рейтинге успешно обновлено!"
        return render(request, 'school/additional_information/main.html', context)
    return render(request, 'school/additional_information/main.html', context)


@login_required
def add_average_marks_for_exam(request, school_id):
    profile = Profile.objects.get(user=request.user)
    school = School.objects.get(id=school_id)

    # Проверка на то, что пользователь является администратором школы
    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    context = {
        'school': school
    }

    subjects = {
        "average_mark_russian": "Русский язык",
        "average_mark_math": "Математика",
        "average_mark_physics": "Физика",
        "average_mark_chemistry": "Химия",
        "average_mark_ikt": "Информатика",
        "average_mark_social": "Обществознание",
        "average_mark_history": "История",
        "average_mark_geo": "География",
        "average_mark_bio": "Биология",
        "average_mark_reading": "Литература",
        "average_mark_english": "Английский язык",
        "average_mark_language": "Иностранный язык"
    }

    if request.method == "POST":
        average_marks = {}
        for key in subjects.keys():
            average_mark = request.POST.get(key)
            average_marks[key] = average_mark

        average_marks = {key: val for key, val in average_marks.items() if val != ''}
        for key in average_marks.keys():
            if AverageMarksForExam.objects.filter(Q(school=school), Q(subject=subjects[key])).exists():
                old_mark = AverageMarksForExam.objects.get(Q(school=school), Q(subject=subjects[key]))
                old_mark.delete()
            new_mark = AverageMarksForExam(school=school, subject=subjects[key], average_mark=average_marks[key])
            new_mark.save()

        context['success_text'] = "Показатели успешно обновлены. Их можно увидеть на главной странице школы!"
        return render(request, 'school/additional_information/add_average_marks_for_exam.html', context)
    return render(request, 'school/additional_information/add_average_marks_for_exam.html', context)


@login_required
def add_info_about_olympics(request, school_id):
    profile = Profile.objects.get(user=request.user)
    school = School.objects.get(id=school_id)

    # Проверка на то, что пользователь является администратором школы
    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    context = {
        'school': school,
        'olympics': InfoAboutOlympics.objects.all().filter(school=school)
    }

    if request.method == "POST":
        name_of_olympic = request.POST.get('name_of_olympic')
        subject = request.POST.get('subject')
        name_of_student = request.POST.get('name_of_student')
        class_of_student = request.POST.get('class_of_student')
        place_in_olympic = request.POST.get('place_in_olympic')

        if is_value_empty(name_of_olympic) or is_value_empty(subject) or is_value_empty(
                name_of_student) or is_value_empty(class_of_student) or is_value_empty(place_in_olympic):
            context['error_text'] = "Все поля обязательны для заполнения! Попробуйте еще раз!"
            return render(request, 'school/additional_information/add_info_about_olympics.html', context)

        new_olympic = InfoAboutOlympics(school=school, name_of_olympic=name_of_olympic, subject=subject,
                                        name_of_student=name_of_student, class_of_student=class_of_student,
                                        place_in_olympic=place_in_olympic)
        new_olympic.save()

        context['success_text'] = "Информация успешно обновлена!"
        context['olympics'] = InfoAboutOlympics.objects.all().filter(school=school)
        return render(request, 'school/additional_information/add_info_about_olympics.html', context)
    return render(request, 'school/additional_information/add_info_about_olympics.html', context)


@login_required
def create_year_of_university(request, school_id):
    profile = Profile.objects.get(user=request.user)
    school = School.objects.get(id=school_id)

    # Проверка на то, что пользователь является администратором школы
    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    context = {
        'school': school,
        'years': UniversityYear.objects.all().filter(school=school)
    }

    if request.method == "POST":
        year = request.POST.get('year')

        if is_value_empty(year):
            context['error_text'] = "Заполните все поля!"
            return render(request, 'school/additional_information/create_year_of_university.html', context)

        if UniversityYear.objects.filter(Q(school=school), Q(year=year)).exists():
            context['error_text'] = "Такой год поступления уже есть!"
            return render(request, 'school/additional_information/create_year_of_university.html', context)

        new_university_year = UniversityYear(school=school, year=year)
        new_university_year.save()
        context['success_text'] = "Год поступления успешно сохранен! "
        return render(request, 'school/additional_information/create_year_of_university.html', context)
    return render(request, 'school/additional_information/create_year_of_university.html', context)


@login_required
def create_stats_by_each_university(request, school_id, year):
    profile = Profile.objects.get(user=request.user)
    school = School.objects.get(id=school_id)

    # Проверка на то, что пользователь является администратором школы
    if is_profile_can_manage_school(profile, school) is False:
        return redirect('error_403')

    year_for_stats = UniversityYear.objects.get(Q(school=school), Q(year=year))

    context = {
        'school': school,
        'year': year_for_stats,
        'university_for_year': StatsByUniversityInOneYear.objects.all().filter(Q(school=school), Q(year=year_for_stats))
    }

    if request.method == "POST":
        university = request.POST.get('university')
        count_students = request.POST.get('count_students')

        if is_value_empty(university) or is_value_empty(count_students):
            context['error_text'] = "Заполните все поля!"
            return render(request, 'school/additional_information/create_stats_by_each_university.html', context)

        new_stats = StatsByUniversityInOneYear(year=year_for_stats, school=school, university=university,
                                               count_people=count_students)
        new_stats.save()
        context['success_text'] = "Информация успешно сохранена!"
    return render(request, 'school/additional_information/create_stats_by_each_university.html', context)
