from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import redirect, render
import random
from network.forms import SignUpForm
from network.models.profile import Profile
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template


def signup(request):
    """
    Страница регистрации пользователя
    :param request: запрос на страницу
    :return: html-страница
    """
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)

            new_id = 1
            if len(User.objects.all()) != 0:
                new_id = User.objects.latest('id').id + 1

            user.username = "id" + str(new_id)
            user.save()

            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)

            profile = Profile(user=user, code=random.randint(100000, 999999), role='USER')
            profile.save()

            # Отправка почты
            d = {
                'first_name': profile.user.first_name,
                'code': profile.code
            }
            plaintext = get_template('email/signup.txt').render(d)
            htmly = get_template('email/signup.html').render(d)

            subject, from_email, to = 'Социальная сеть для школ и учеников', 'course.work.bpi196.network@gmail.com', \
                                      profile.user.email
            text_content = plaintext
            html_content = htmly
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()

            login(request, user)
            return redirect('index_page')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})
