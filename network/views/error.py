from django.shortcuts import render


def error_403(request):
    """
    Функция, которая возвращает ошибку 403 (недостаточно прав)
    :param request: запрос на страницу
    :return: html-страница
    """
    return render(request, 'http_codes/403.html', {})
