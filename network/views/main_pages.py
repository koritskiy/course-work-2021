import random

from django.contrib.auth.models import User
from django.shortcuts import render

from network.models import Profile


def index_page(request):
    """
    Функция, которая возвращает главную страницу
    :param request: запрос на страницу
    :return: html-страница
    """
    context = {
        'users': User.objects.all()
    }

    if request.user.is_authenticated:
        if not Profile.objects.filter(user=request.user).exists():
            new_profile = Profile(user=request.user, role='USER', code=random.randint(100000, 999999))
            new_profile.save()
    return render(request, 'index.html', context)


def faq(request):
    """
    Функция, которая возвращает страницу часто задаваемых вопросов
    :param request: запрос на страницу
    :return: html-страница
    """
    return render(request, 'faq.html', {})
