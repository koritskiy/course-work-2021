from django.db import models


class Student(models.Model):
    """
    Модель ученика, появляется как только ученик добавил всю необходимую информацию
    (школу, город, класс и предметы)
    """
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE)

    city = models.CharField(max_length=256)
    school = models.CharField(max_length=256)
    school_class = models.ForeignKey('SchoolClass', on_delete=models.CASCADE)

    class Meta:
        db_table = 'student'
        app_label = 'network'


class SubjectAndStudent(models.Model):
    """
    Модель для связи студента с предметом
    """
    subject = models.CharField(max_length=256)
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE)

    class Meta:
        db_table = 'subject_and_student'
        app_label = 'network'


class SchoolAndStudent(models.Model):
    """
    Модель для связи существующей школы на сайте с учеником
    """
    student = models.ForeignKey('Student', on_delete=models.CASCADE)
    school = models.ForeignKey('School', on_delete=models.CASCADE)

    class Meta:
        db_table = "school_and_student"
        app_label = "network"


class RequestFromParent(models.Model):
    """
    Модель заявок от родителей на причастность к ученику
    """
    parent = models.ForeignKey('Parent', on_delete=models.CASCADE)
    student = models.ForeignKey('Student', on_delete=models.CASCADE)
    status = models.CharField(max_length=256, default="REVIEW")

    class Meta:
        db_table = 'request_from_parent'
        app_label = 'network'


class StudentToParent(models.Model):
    """
    Модель связи ученика и родителя
    """
    parent = models.ForeignKey('Parent', on_delete=models.CASCADE)
    student = models.ForeignKey('Student', on_delete=models.CASCADE)

    class Meta:
        db_table = 'student_to_parent'
        app_label = 'network'
