from django.db import models


class School(models.Model):
    """
    Модель школы
    """
    short_name = models.CharField(max_length=256)
    full_name = models.CharField(max_length=256)
    number = models.CharField(max_length=256)
    address = models.CharField(max_length=512)
    phone_coordinator = models.CharField(max_length=256)
    phone = models.CharField(max_length=256)
    email = models.CharField(max_length=256)
    country = models.CharField(max_length=256)
    city = models.CharField(max_length=256)
    coords_longitude = models.FloatField()
    coords_latitude = models.FloatField()
    photo = models.FileField(upload_to='schools/', null=True, blank=True)
    is_approve = models.BooleanField(default=False)
    place_in_rating = models.IntegerField()

    creator = models.ForeignKey('Profile', on_delete=models.CASCADE)

    class Meta:
        db_table = 'school'
        app_label = 'network'


class SchoolAndSubjects(models.Model):
    """
    Моделька для профильных предметов школы
    """
    subject = models.CharField(max_length=256)
    school = models.ForeignKey('School', on_delete=models.CASCADE)

    class Meta:
        db_table = 'schools_and_subjects'
        app_label = 'network'


class SchoolManage(models.Model):
    """
    Модель для сохранения администраторов школы
    """
    school = models.ForeignKey('School', on_delete=models.CASCADE)
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE)

    class Meta:
        db_table = 'school_manage'
        app_label = 'network'


class RequestToSchool(models.Model):
    """
    Запросы на присоединение к школе
    """
    school = models.ForeignKey('School', on_delete=models.CASCADE)
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE)
    teacher = models.ForeignKey('Teacher', on_delete=models.CASCADE, null=True, blank=True)
    student = models.ForeignKey('Student', on_delete=models.CASCADE, null=True, blank=True)
    status = models.CharField(max_length=256, default="REVIEW")

    class Meta:
        db_table = 'request_to_school'
        app_label = 'network'


class AverageMarksForExam(models.Model):
    """
    Таблица хранения средних баллов по каждому предмету по ЕГЭ
    """
    school = models.ForeignKey('School', on_delete=models.CASCADE)

    subject = models.CharField(max_length=512)
    average_mark = models.IntegerField()

    class Meta:
        db_table = 'average_marks_for_exam'
        app_label = 'network'


class InfoAboutOlympics(models.Model):
    """
    Таблица хранения информации об успехах в олимпиадах учениками школы
    """
    school = models.ForeignKey('School', on_delete=models.CASCADE)

    name_of_olympic = models.CharField(max_length=1024)
    subject = models.CharField(max_length=512)
    name_of_student = models.CharField(max_length=512)
    class_of_student = models.IntegerField()
    place_in_olympic = models.CharField(max_length=512)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'info_about_olympics'
        app_label = 'network'


class UniversityYear(models.Model):
    """
    Таблица для хранения года поступления в университет для каждой школы
    """
    school = models.ForeignKey('School', on_delete=models.CASCADE)
    year = models.IntegerField()

    class Meta:
        db_table = 'university_year'
        app_label = 'network'


class StatsByUniversityInOneYear(models.Model):
    """
    Таблица для хранения статистики поступления в конкретные университеты в конкретный год
    """
    year = models.ForeignKey('UniversityYear', on_delete=models.CASCADE)
    school = models.ForeignKey('School', on_delete=models.CASCADE, blank=True, null=True)

    university = models.CharField(max_length=1024)
    count_people = models.IntegerField()

    class Meta:
        db_table = 'stats_by_university_in_one_year'
        app_label = 'network'
