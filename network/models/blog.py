from django.db import models
from tinymce.models import HTMLField


class Post(models.Model):
    """
    Модель посты в ленте школы
    """
    title = models.CharField(max_length=512)
    content = HTMLField()
    created_at = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey('Profile', on_delete=models.CASCADE)
    photo = models.FileField(upload_to='posts/', null=True, blank=True)
    school = models.ForeignKey('School', on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        db_table = 'post'
        app_label = 'network'


class Comment(models.Model):
    """
    Модель комментариев к посту
    """
    content = models.CharField(max_length=1024)
    author = models.ForeignKey('Profile', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey('Post', on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        db_table = 'comment'
        app_label = 'network'
