from django.db import models


class Administration(models.Model):
    """
    Модель любого представителя администрации школы
    """
    middle_name = models.CharField(max_length=256)
    position = models.CharField(max_length=256)
    city = models.CharField(max_length=256, blank=True, default=True)
    school = models.ForeignKey('School', on_delete=models.CASCADE, blank=True, null=True)
    photo = models.FileField(upload_to='administration/passports/', null=True, blank=True)
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE)

    class Meta:
        db_table = 'administration'
        app_label = 'network'
