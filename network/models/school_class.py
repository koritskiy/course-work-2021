from django.contrib.auth.models import User
from django.db import models


class SchoolClass(models.Model):
    """
    Школьный класс
    user - пользователь-ученик (роль = ученик)
    number - номер класс (1-11)
    letter - буква класса
    """
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE)
    number = models.IntegerField()
    letter = models.CharField(max_length=4)
    school = models.ForeignKey('School', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        db_table = 'school_class'
        app_label = 'network'
