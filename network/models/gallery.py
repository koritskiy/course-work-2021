from django.db import models


class Folder(models.Model):
    """
    Модель вкладки для фотогалереи
    """
    title = models.CharField(max_length=512)
    created_at = models.DateTimeField(auto_now_add=True)
    cover = models.FileField(upload_to='gallery/', null=True, blank=True)
    author = models.ForeignKey('Profile', on_delete=models.CASCADE)
    school = models.ForeignKey('School', on_delete=models.CASCADE)

    class Meta:
        db_table = 'folder'
        app_label = 'network'


class ImageForFolder(models.Model):
    """
    Модель для фотографии внутри фотогалереи
    """
    folder = models.ForeignKey('Folder', on_delete=models.CASCADE)
    image = models.FileField(upload_to='gallery/')

    class Meta:
        db_table = 'image_for_folder'
        app_label = 'network'
