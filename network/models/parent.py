from django.db import models


class Parent(models.Model):
    """
    Модель родителя
    """
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE)
    city = models.CharField(max_length=256)
    photo = models.FileField(upload_to='parents/', null=True, blank=True)

    class Meta:
        db_table = 'parent'
        app_label = 'network'


class ParentAndSchool(models.Model):
    """
    Модель привязки родителя к школе
    """
    parent = models.ForeignKey('Parent', on_delete=models.CASCADE)
    school = models.ForeignKey('School', on_delete=models.CASCADE)

    class Meta:
        db_table = 'parent_and_school'
        app_label = 'network'
