from django.db import models


class LikeForPost(models.Model):
    """
    Модель, которая сохраняет лайки к постам
    """
    post = models.ForeignKey('Post', on_delete=models.CASCADE)
    school = models.ForeignKey('School', on_delete=models.CASCADE, null=True, blank=True)
    author = models.ForeignKey('Profile', on_delete=models.CASCADE)

    class Meta:
        db_table = 'like_for_post'
        app_label = 'network'


class RateComment(models.Model):
    """
    Модель, которая сохраняет голос к комментарию
    """
    comment = models.ForeignKey('Comment', on_delete=models.CASCADE)
    author = models.ForeignKey('Profile', on_delete=models.CASCADE)
    is_plus = models.BooleanField()

    class Meta:
        db_table = 'rate_comment'
        app_label = 'network'
