from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    """
    Профиль, расширяет модель пользователя по умолчанию
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    code = models.IntegerField()
    is_email_confirm = models.BooleanField(default=False)
    role = models.CharField(max_length=256)
    is_role_confirm = models.BooleanField(default=None, null=True, blank=True)
    photo = models.FileField(upload_to='users/', null=True, blank=True)

    class Meta:
        db_table = 'profile'
        app_label = 'network'
