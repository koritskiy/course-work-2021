from django.db import models


class Teacher(models.Model):
    """
    Модель любого учителя
    """
    middle_name = models.CharField(max_length=256)
    city = models.CharField(max_length=256, blank=True, default=True)
    school = models.ForeignKey('School', on_delete=models.CASCADE, blank=True, null=True)
    file = models.FileField(upload_to='teachers/resumes/', null=True, blank=True)
    photo = models.FileField(upload_to='teachers/passports/', null=True, blank=True)
    profile = models.ForeignKey('Profile', on_delete=models.CASCADE)

    class Meta:
        db_table = 'teacher'
        app_label = 'network'


class TeacherAndSubject(models.Model):
    """
    Модель учителя и его предмета
    """
    teacher = models.ForeignKey('Profile', on_delete=models.CASCADE)
    subject = models.CharField(max_length=256)

    class Meta:
        db_table = 'teacher_and_subject'
        app_label = 'network'


class TeacherAndClass(models.Model):
    """
    Для классных руководителей
    """
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    school_class = models.ForeignKey('SchoolClass', on_delete=models.CASCADE)

    class Meta:
        db_table = 'teacher_and_class'
        app_label = 'network'


class CommentForTeacher(models.Model):
    """
    Таблица для хранения комментариев об учителе
    """
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    comment = models.CharField(max_length=2048)
    author = models.ForeignKey('Profile', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'comment_for_teacher'
        app_label = 'network'
