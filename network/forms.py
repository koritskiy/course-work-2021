from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.validators import EmailValidator
from django import forms


class SignUpForm(UserCreationForm):
    """
    Класс формы для регистрации:
        first_name - имя
        last_name - фамилия
        email - email пользователя
    """
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.EmailField(max_length=254, required=True)

    error_messages = {
        'password_mismatch': "Пароли не совпадают, попробуйте еще раз.",
    }

    class Meta:
        """
        используемый модуль - User
        поля регистрации - fields
        """
        model = User
        fields = ('first_name', 'last_name', 'email', 'password1', 'password2')

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['email'].validators = [EmailValidator(message="Некорректный адрес электронной почты.")]

        for field in self.fields.values():
            field.error_messages = {'required': 'Это поле необходимо для заполнения.'}

    def clean_email(self):
        """
        Проверяет существует ли пользователь с такой почтой, как ввел новый пользователь
        """
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('Пользователь с такой почтой уже существует!')
        return email
