"""CourseWork URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.contrib import admin
from network import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index_page, name='index_page'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('faq/', views.faq, name='faq'),

    # Регистрация
    path('accounts/signup/', views.signup, name='signup_page'),
    path('accounts/forget/password/', views.forget_password, name='forget_password'),

    # Аккаунт
    path('account/', views.account, name='account'),
    path('account/get/information/student/', views.get_info_about_student, name='get_info_about_student'),
    path('account/get/information/parent/', views.get_info_about_parent, name='get_info_about_parent'),
    path('account/get/information/teacher/', views.get_info_about_teacher, name='get_info_about_teacher'),
    path('account/get/information/administration/', views.get_info_about_administration,
         name='get_info_about_administration'),
    path('profile/<str:user_id>/', views.each_user, name='each_user'),

    # Изменение данных в аккаунте
    path('account/settings/edit/email/', views.edit_email, name='edit_email'),
    path('account/settings/edit/password/', views.edit_password, name='edit_password'),
    path('account/settings/edit/avatar/', views.edit_avatar, name='edit_avatar'),
    path('account/settings/confirm/email/', views.confirm_email, name='confirm_email'),
    path('account/settings/edit/student/', views.edit_student, name='edit_student'),
    path('account/settings/edit/teacher/', views.edit_teacher, name='edit_teacher'),
    path('account/settings/send/code/again/<str:email>/', views.send_email_code_again, name='send_email_code_again'),

    # Присоединение ребенка к родителю
    path('account/add/child/', views.choose_child, name='add_child'),
    path('account/request/to/child/<int:profile_id>/', views.request_to_child, name='request_to_child'),
    path('account/request/from/parent/approve/<int:request_id>/', views.approve_parent_from_child,
         name='approve_parent_from_child'),
    path('account/request/from/parent/reject/<int:request_id>/', views.reject_parent_from_child,
         name='reject_parent_from_child'),

    # Ошибки
    path('error/403', views.error_403, name='error_403'),

    # Заявки на подтверждение аккаунта
    path('request/user/all/', views.all_requests_for_approve_users, name='all_requests_for_approve_users'),
    path('request/user/approve/<int:profile_id>/', views.approve_request_for_user, name='approve_request_for_user'),
    path('request/user/reject/<int:profile_id>/', views.reject_request_for_user, name='reject_request_for_user'),

    # Заявки на подтверждение школы
    path('request/school/all/', views.all_requests_for_approve_school, name='all_requests_for_approve_schools'),
    path('request/school/approve/<int:school_id>/', views.approve_request_for_school,
         name='approve_request_for_school'),
    path('request/school/reject/<int:school_id>/', views.reject_request_for_school, name='reject_request_for_school'),

    # Школы
    path('school/create/', views.create_school, name='create_school'),
    path('school/<int:school_id>/', views.each_school, name='each_school'),
    path('schools/', views.all_schools, name='all_schools'),
    path('school/settings/add/admin/<int:school_id>/', views.add_admin_for_school, name='add_admin_for_school'),
    path('school/settings/make/admin/<int:school_id>/<int:profile_id>/', views.make_admin_for_school,
         name='make_admin_for_school'),
    path('school/settings/edit/<int:school_id>/', views.edit_school, name='edit_school'),
    path('school/settings/add/info/<int:school_id>/', views.add_additional_info_for_school,
         name='add_additional_info_for_school'),
    path('school/settings/add/average/marks/exam/<int:school_id>/', views.add_average_marks_for_exam,
         name='add_average_marks_for_exam'),
    path('school/settings/add/info/olympics/<int:school_id>/', views.add_info_about_olympics,
         name='add_info_about_olympics'),
    path('school/show/all/info/olympics/<int:school_id>/', views.show_all_info_about_olympics,
         name='show_all_info_about_olympics'),
    path('school/settings/create/year/of/university/<int:school_id>/', views.create_year_of_university,
         name='create_year_of_university'),
    path('school/settings/create/stats/by/university/<int:school_id>/<int:year>/',
         views.create_stats_by_each_university, name='create_stats_by_each_university_in_one_year'),

    # Присоединение к школе
    path('school/join/', views.join_school, name='join_school'),
    path('school/requests/join/<int:school_id>/', views.requests_for_joining_school,
         name='requests_for_joining_school'),
    path('school/requests/join/approve/<int:school_id>/<int:request_id>/', views.approve_request_for_join_school,
         name='approve_request_for_join_school'),
    path('school/requests/join/reject/<int:school_id>/<int:request_id>/', views.reject_request_for_join_school,
         name='reject_request_for_join_school'),

    # Управление лентой школы
    path('school/add/post/<int:school_id>/', views.add_post, name='add_post'),
    path('school/blog/<int:school_id>/', views.all_posts_for_school, name='all_posts_for_school'),
    path('school/blog/like/<int:school_id>/<int:post_id>/', views.like_for_post, name='like_for_post'),
    path('school/blog/unlike/<int:school_id>/<int:post_id>/', views.unlike_for_post, name='unlike_for_post'),
    path('school/blog/post/<int:school_id>/<int:post_id>/', views.each_post, name='each_post'),
    path('school/blog/comment/plus/<int:comment_id>/', views.plus_for_comment, name='plus_for_comment'),
    path('school/blog/comment/minus/<int:comment_id>/', views.minus_for_comment, name='minus_for_comment'),
    path('school/blog/edit/post/<int:school_id>/<int:post_id>/', views.edit_post, name='edit_post'),
    path('school/blog/delete/post/<int:school_id>/<int:post_id>/', views.delete_post, name='delete_post'),

    # Фотогалерея
    path('school/add/images/<int:school_id>/', views.add_folder, name='add_folder'),
    path('school/gallery/<int:school_id>/<int:folder_id>/', views.each_folder, name='each_folder'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
